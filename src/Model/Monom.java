package Model;

public class Monom {
	
	private double coeficient;
	private int grad;
	
	public Monom() {
		
	}
	
	public Monom(double coeficient, int grad) {
		setCoeficient(coeficient);
		setGrad(grad);
	}
	
	public void setCoeficient(double coeficient) {
		this.coeficient = coeficient;
	}
	
	public void setGrad(int grad) {
		this.grad = grad;
	}
	
	public double getCoeficient() {
		return coeficient;
	}
	
	public int getGrad() {
		return grad;
	}
	
	public String toString() {
		String s = "";
		if (this.grad > 1) {
			if (this.coeficient < 0)
				s = this.coeficient + "X^" + this.grad;
			else
				s = "+" + this.coeficient + "X^" + this.grad;
		}
		else if (this.grad == 1) {
			if (this.coeficient < 0)
				s = this.coeficient + "X";
			else
				s = "+" + this.coeficient + "X";
		}
		else {
			if (this.coeficient < 0)
				s += this.coeficient;
			else
				s = "+" + this.coeficient + "";
		}
		return s;
	}
}
