package Model;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MonomTest {

	@Test
	void testMonom() {
			
	}

	@Test
	void testMonomDoubleInt() {
		Monom m = new Monom(10, 2);
		assertTrue("Error", m.getCoeficient() == 10 && m.getGrad() == 2);
		AssertionError eroare = new AssertionError();
	}

	@Test
	void testSetCoeficient() {
		Monom m = new Monom();
		m.setCoeficient(10);
		assertTrue("Error", m.getCoeficient() == 10);
		AssertionError eroare = new AssertionError();
	}

	@Test
	void testSetGrad() {
		Monom m = new Monom();
		m.setGrad(2);
		assertTrue("Error", m.getGrad() == 2);
		AssertionError eroare = new AssertionError();
	}

	@Test
	void testGetCoeficient() {
		Monom m = new Monom();
		m.setCoeficient(7.7);
		double coef = m.getCoeficient();
		assertTrue("Error", coef == 7.7);
		AssertionError eroare = new AssertionError();
	}

	@Test
	void testGetGrad() {
		Monom m = new Monom();
		m.setGrad(3);
		int grad = m.getGrad();
		assertTrue("Error", grad == m.getGrad());
		AssertionError eroare = new AssertionError();
	}


	@Test
	void testToString() {
		Monom m = new Monom();
		m.setCoeficient(7);
		m.setGrad(2);
		assertTrue("Error", m.toString().equals("+7.0X^2"));
		AssertionError eroare = new AssertionError();
	}

}
