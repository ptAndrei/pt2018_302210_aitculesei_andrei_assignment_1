package Model;

import java.util.ArrayList;
import java.util.List;

public class Polinom {

	public List<Monom> monoame;
	
	public Polinom() {
		monoame = new ArrayList<Monom>();
	}
	
	//Adaugare monoame in "lista" unui polinom
	public void adaugareLista(Monom monom) {
		
		boolean diferit = true;
		
		for (Monom m : this.monoame) {
			if (m.getGrad() == monom.getGrad()) {
				m.setCoeficient(m.getCoeficient() + monom.getCoeficient());
				if (m.getCoeficient() == 0)
					this.monoame.remove(m);
				diferit = false;
				break;
			}
		}
		
		if (diferit == true) {
			this.monoame.add(monom);
		}
	}
	
	public static Monom gradMaxim(Polinom p) {
		
		int max = 0;
		Monom monomMaxim = new Monom();
		
		for(Monom m : p.monoame) {
			if (m.getGrad() > max) {
				max = m.getGrad();
				monomMaxim = m;
			}
		}
		
		return monomMaxim;
	}
	
	public Polinom copierePolinom() {
		
		Polinom p = new Polinom();
		
		for (Monom m : this.monoame) {
			if (m.getCoeficient() != 0)
				p.adaugareLista(new Monom(m.getCoeficient(), m.getGrad()));
		}
		
		return p;
	}
	
	//Operatii efectuate cu ajutorul polinoamelor
	public Polinom aduna(Polinom p) {
		
		Polinom polRezultat = this.copierePolinom();
		
		for (Monom m : p.monoame)
			polRezultat.adaugareLista(m);
		
		return polRezultat;
	}
	
	public Polinom scade(Polinom p) {
		
		Polinom polRezultat = this.copierePolinom();
		Polinom polScazut = p.copierePolinom();
		
		for (Monom m : polScazut.monoame) {
			m.setCoeficient(m.getCoeficient() * (-1));
			polRezultat.adaugareLista(m);
		}
		
		polRezultat = polRezultat.copierePolinom();
		return polRezultat;
	}
	
	public Polinom inmulteste(Polinom p) {
		
		Polinom polRezultat = new Polinom();
		Polinom polInmultit = p.copierePolinom();
		
		for(Monom m1 : this.monoame) {
			for(Monom m2 : polInmultit.monoame)
				polRezultat.adaugareLista(new Monom(m1.getCoeficient() * m2.getCoeficient(), m1.getGrad() + m2.getGrad()));
		}
		
		polRezultat = polRezultat.copierePolinom();
		return polRezultat;
	}
	
	public Polinom imparte(Polinom p) {
		
		Polinom polRezultat = new Polinom();
		Polinom rest;
		Polinom deimpartit = this.copierePolinom();
		Monom gradMaximDeimpartit = Polinom.gradMaxim(this);
		Monom gradMaximImpartitor = Polinom.gradMaxim(p);
		Monom monomRezultat;
		
		while (gradMaximDeimpartit.getGrad() >= gradMaximImpartitor.getGrad()) {
			
			rest = new Polinom();
			monomRezultat = new Monom();
			
			monomRezultat.setCoeficient(gradMaximDeimpartit.getCoeficient() / gradMaximImpartitor.getCoeficient());
			monomRezultat.setGrad(gradMaximDeimpartit.getGrad() - gradMaximImpartitor.getGrad());
			polRezultat.adaugareLista(monomRezultat);
			for(Monom m : p.monoame)
				rest.adaugareLista(new Monom((monomRezultat.getCoeficient() * m.getCoeficient()) * (-1), monomRezultat.getGrad() + m.getGrad()));
			rest = rest.copierePolinom();
			deimpartit = deimpartit.aduna(rest);
			gradMaximDeimpartit = Polinom.gradMaxim(deimpartit);
		}
		return polRezultat;
	}
	
	public static Polinom derivata(Polinom p) {
		
		Polinom polRezultat = new Polinom();
		
		for (Monom m : p.monoame) {
			if (m.getGrad() > 0) {
				m.setCoeficient(m.getCoeficient() * m.getGrad());
				m.setGrad(m.getGrad() - 1);
				polRezultat.adaugareLista(m);
			}
		}
		
		return polRezultat;
	}
	
	public static Polinom integrala(Polinom p) {
		
		Polinom polRezultat = new Polinom();
		
		for(Monom m : p.monoame) {
			
			if (m.getGrad() > 0) {
				m.setCoeficient(m.getCoeficient() / (m.getGrad() + 1));
				m.setGrad(m.getGrad() + 1);
			}
			else if (m.getGrad() == 0) {
				m.setGrad(m.getGrad() + 1);
			}
			polRezultat.adaugareLista(m);
		}
		
		return polRezultat;
	}
	
	//Afisare polinom
	public String toString() {
		String polinom = "";
		for(Monom m : this.monoame) {
			if (m.getGrad() > 1) {
				if (m.getCoeficient() < 0)
					polinom += m.getCoeficient() + "X^" + m.getGrad();
				else if (m.getCoeficient() != 0)
						polinom += "+" + m.getCoeficient() + "X^" + m.getGrad();
			}
			else if (m.getGrad() == 1) {
				if (m.getCoeficient() < 0)
					polinom += m.getCoeficient() + "X";
				else
					polinom += "+" + m.getCoeficient() + "X";
			}
			else {
				if (m.getCoeficient() < 0)
					polinom += m.getCoeficient() + "";
				else
					polinom += "+" + m.getCoeficient() + "";
			}
		}
		
		return polinom;
	}
}
