package Control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import Model.*;
import View.*;

public class Control {
	
	private Fereastra view;
	private Polinom polinomRezultat;
	
	public Control(Fereastra view) {
		
		this.view = view;
		control();
	}
	
	public Polinom convertireString(String polinom) {
		
		Polinom polRezultat = new Polinom();
		int index = 0;

		Monom m = new Monom();
		String[] monoame = polinom.split("(?=[-+]+)");
		for (String monom : monoame) {

			String[] numere = monom.split("[X^]+");
			for (String numar : numere) {

				if (index % 2 == 0) {
					m.setCoeficient(Double.parseDouble(numar));
				}
				else
					m.setGrad(Integer.parseInt(numar));

				if (index % 2 != 0) {
					polRezultat.adaugareLista(m);
					m = new Monom();
				}
				index++;
			}
		}
		return polRezultat;
	}
	
	public void control(){
		
		view.btnAdunare.addActionListener(new ActionListener() {	
			public void actionPerformed(ActionEvent e) {
				
				String polinom1 = view.textPolinom1.getText();
				String polinom2 = view.textPolinom2.getText();
				Polinom pol1 = new Polinom();
				Polinom pol2 = new Polinom();
				pol1 = convertireString(polinom1);
				pol2 = convertireString(polinom2);
				
				polinomRezultat = pol1.aduna(pol2);
				view.textPolinomRezultat.setText(polinomRezultat.toString());
			}
		});
		
		view.btnScadere.addActionListener(new ActionListener() {	
			public void actionPerformed(ActionEvent e) {
				
				String polinom1 = view.textPolinom1.getText();
				String polinom2 = view.textPolinom2.getText();
				Polinom pol1 = new Polinom();
				Polinom pol2 = new Polinom();
				pol1 = convertireString(polinom1);
				pol2 = convertireString(polinom2);
				
				polinomRezultat = pol1.scade(pol2);
				view.textPolinomRezultat.setText(polinomRezultat.toString());
			}
		});
		
		view.btnInmultire.addActionListener(new ActionListener() {	
			public void actionPerformed(ActionEvent e) {
				
				String polinom1 = view.textPolinom1.getText();
				String polinom2 = view.textPolinom2.getText();
				Polinom pol1 = new Polinom();
				Polinom pol2 = new Polinom();
				pol1 = convertireString(polinom1);
				pol2 = convertireString(polinom2);
				
				polinomRezultat = pol1.inmulteste(pol2);
				view.textPolinomRezultat.setText(polinomRezultat.toString());
			}
		});
		
		view.btnImpartire.addActionListener(new ActionListener() {	
			public void actionPerformed(ActionEvent e) {
				
				String polinom1 = view.textPolinom1.getText();
				String polinom2 = view.textPolinom2.getText();
				Polinom pol1 = new Polinom();
				Polinom pol2 = new Polinom();
				pol1 = convertireString(polinom1);
				pol2 = convertireString(polinom2);
				
				polinomRezultat = pol1.imparte(pol2);
				view.textPolinomRezultat.setText(polinomRezultat.toString());
			}
		});
		
		view.btnDerivarePolinom1.addActionListener(new ActionListener() {	
			public void actionPerformed(ActionEvent e) {
				
				String polinom1 = view.textPolinom1.getText();
				Polinom pol1 = new Polinom();
				pol1 = convertireString(polinom1);
				
				polinomRezultat = Polinom.derivata(pol1);
				view.textPolinomRezultat.setText(polinomRezultat.toString());
			}
		});
		
		view.btnDerivarePolinom2.addActionListener(new ActionListener() {	
			public void actionPerformed(ActionEvent e) {
				
				String polinom2 = view.textPolinom2.getText();
				Polinom pol2 = new Polinom();
				pol2 = convertireString(polinom2);
				
				polinomRezultat = Polinom.derivata(pol2);
				view.textPolinomRezultat.setText(polinomRezultat.toString());
			}
		});
		
		view.btnIntegrarePolinom1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String polinom1 = view.textPolinom1.getText();
				Polinom pol1 = new Polinom();
				pol1 = convertireString(polinom1);
				
				polinomRezultat = Polinom.integrala(pol1);
				view.textPolinomRezultat.setText(polinomRezultat.toString());
			}
		});
		
		view.btnIntegrarePolinom2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String polinom2 = view.textPolinom2.getText();
				Polinom pol2 = new Polinom();
				pol2 = convertireString(polinom2);
				
				polinomRezultat = Polinom.integrala(pol2);
				view.textPolinomRezultat.setText(polinomRezultat.toString());
			}
		});
		
		view.btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				view.textPolinom1.setText(null);
				view.textPolinom2.setText(null);
				view.textPolinomRezultat.setText(null);
			}
		});
		
		view.btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				System.exit(0);
			}
		});
	}
}
