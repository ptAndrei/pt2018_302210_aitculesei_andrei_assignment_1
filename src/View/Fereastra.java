package View;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.GridLayout;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Fereastra {

	private JFrame fereastra;
	
	private JPanel panouInserarePolinoame;
	private JPanel panouSelectareOperatii;
	private JPanel panouAfisareRezultat;
	
	private JLabel labelPolinom1;
	private JLabel labelPolinom2;
	private JLabel labelRezultat;
	
	public JTextField textPolinom1;
	public JTextField textPolinom2;
	public JTextField textPolinomRezultat;
	
	public JButton btnAdunare;
	public JButton btnScadere;
	public JButton btnInmultire;
	public JButton btnImpartire;
	public JButton btnDerivarePolinom1;
	public JButton btnDerivarePolinom2;
	public JButton btnIntegrarePolinom1;
	public JButton btnIntegrarePolinom2;
	public JButton btnClear;
	public JButton btnExit;

	public Fereastra() {
		fereastra = new JFrame("Operatii polinoame");
		fereastra.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fereastra.setSize(500, 500);
		
		panouInserarePolinoame = new JPanel();
		panouSelectareOperatii = new JPanel(new GridBagLayout());
		panouAfisareRezultat = new JPanel();

		labelPolinom1 = new JLabel("Polinom 1:");
		labelPolinom2 = new JLabel("Polinom 2:");
		labelRezultat = new JLabel("Rezultat:");
		
		textPolinom1 = new JTextField();
		textPolinom2 = new JTextField();
		textPolinomRezultat = new JTextField();
		
		btnAdunare = new JButton("+");
		btnScadere = new JButton("-");
		btnInmultire = new JButton("X");
		btnImpartire = new JButton("/");
		btnClear = new JButton("Clear");
		btnExit = new JButton("Exit");
		btnDerivarePolinom1 = new JButton("deriv(p1)");
		btnDerivarePolinom2 = new JButton("deriv(p2)");
		btnIntegrarePolinom1 = new JButton("integ(p1)");
		btnIntegrarePolinom2 = new JButton("integ(p2)");
		
		btnAdunare.setFont(new Font("Arial", Font.BOLD, 35));
		btnAdunare.setSize(10, 10);
		btnScadere.setFont(new Font("Arial", Font.BOLD, 35));
		btnScadere.setSize(10, 10);
		btnInmultire.setFont(new Font("Arial", Font.BOLD, 35));
		btnInmultire.setSize(10, 10);
		btnImpartire.setFont(new Font("Arial", Font.BOLD, 35));
		btnImpartire.setSize(10, 10);
		btnDerivarePolinom1.setFont(new Font("Arial", Font.BOLD, 15));
		btnDerivarePolinom1.setSize(10, 10);
		btnDerivarePolinom2.setFont(new Font("Arial", Font.BOLD, 15));
		btnDerivarePolinom2.setSize(10, 10);
		btnIntegrarePolinom1.setFont(new Font("Arial", Font.BOLD, 15));
		btnIntegrarePolinom1.setSize(10, 10);
		btnIntegrarePolinom2.setFont(new Font("Arial", Font.BOLD, 15));
		btnIntegrarePolinom2.setSize(10, 10);
		
		panouInserarePolinoame.setLayout(new BoxLayout(panouInserarePolinoame, BoxLayout.Y_AXIS));
		panouInserarePolinoame.add(labelPolinom1);
		panouInserarePolinoame.add(textPolinom1);
		panouInserarePolinoame.add(labelPolinom2);
		panouInserarePolinoame.add(textPolinom2);
		fereastra.add(panouInserarePolinoame, BorderLayout.NORTH);
		
		panouSelectareOperatii.setLayout(new GridLayout(2, 4));
		panouSelectareOperatii.add(btnAdunare);
		panouSelectareOperatii.add(btnScadere);
		panouSelectareOperatii.add(btnInmultire);
		panouSelectareOperatii.add(btnImpartire);
		panouSelectareOperatii.add(btnDerivarePolinom1);
		panouSelectareOperatii.add(btnDerivarePolinom2);
		panouSelectareOperatii.add(btnIntegrarePolinom1);
		panouSelectareOperatii.add(btnIntegrarePolinom2);
		fereastra.add(panouSelectareOperatii, BorderLayout.CENTER);
		
		panouAfisareRezultat.setLayout(new BoxLayout(panouAfisareRezultat, BoxLayout.X_AXIS));
		panouAfisareRezultat.add(labelRezultat);
		panouAfisareRezultat.add(textPolinomRezultat);
		panouAfisareRezultat.add(btnClear);
		panouAfisareRezultat.add(btnExit);
		fereastra.add(panouAfisareRezultat, BorderLayout.SOUTH);
		
		fereastra.setVisible(true);
	}
}
